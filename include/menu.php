<nav class="navbar navbar-default">
    <div class="container">

        <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
           <!--  <a class="navbar-brand" href="index.php"><img src="images/asiana-logo.png" alt="asiana-logo" class="img-responsive"></a> -->
        </div>

        <div class="collapse navbar-collapse js-navbar-collapse">
            <ul class="nav navbar-nav">

                <!--  <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li> -->
                <li><a href="<?php echo $base_url;?>index.php" class="active">गृहपृष्ठ </a></li>
                <li><a href="<?php echo $base_url;?>category.php">समाज </a></li>

                <!--mega-menu start-->
                <li class="dropdown mega-dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">राजनीित <i class="fa fa-caret-down" aria-hidden="true"></i></a>

                    <div class="dropdown-menu mega-dropdown-menu dropdown-activities">
                        <div class="row">
                            <div class="col-sm-3 mega-menu-listing">
                                <a href="#">विचार</a>
                            </div>
                            <div class="col-sm-9 side-bar-menu first-show">
                                <div class="row">
                                    <h3>Bishesh Samachar</h3>
                                    <div class="col-md-4 ">            
                                            
                                          <div class="thumbnail-box">
                                          <img src="<?php echo $base_url;?>dist/img/Dr.-KC-in-Hunger-Strike-9-826x495.jpg" alt="new_img" class="img-responsive">
                                          <div class="caption">
                                            <a href="#" class="date-box" role="button"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;१ िदन अगािड</a>
                                            <a href="#" target="_blank"><h4>केसीले आमरण अनसन, उपचार र पढार्इ नरोक्ने (िभिडयोसिहत)</h4></a>
                                            <p class="caption-details">त्रिवि शिक्षण अस्पतालका प्रा. डा. गोविन्द केसीले सोमबार ११ औं आमरण अनसन सुरु गरेका छन् । सरकारले अनसन नबस्न अाग्रह गरे...अस्पतालमा पत्रकार सम्मेलन गरेर ...</p>         
                                          </div>
                                          </div>

                                    </div>
                                     <div class="col-md-4 ">            
                                            
                                          <div class="thumbnail-box">
                                          <img src="<?php echo $base_url;?>dist/img/Dr.-KC-in-Hunger-Strike-9-826x495.jpg" alt="new_img" class="img-responsive">
                                          <div class="caption">
                                            <a href="#" class="date-box" role="button"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;१ िदन अगािड</a>
                                            <a href="#" target="_blank"><h4>केसीले आमरण अनसन, उपचार र पढार्इ नरोक्ने (िभिडयोसिहत)</h4></a>
                                            <p class="caption-details">त्रिवि शिक्षण अस्पतालका प्रा. डा. गोविन्द केसीले सोमबार ११ औं आमरण अनसन सुरु गरेका छन् । सरकारले अनसन नबस्न अाग्रह गरे...अस्पतालमा पत्रकार सम्मेलन गरेर ...</p>         
                                          </div>
                                          </div>

                                    </div>
                                     <div class="col-md-4 ">            
                                            
                                          <div class="thumbnail-box">
                                          <img src="<?php echo $base_url;?>dist/img/Dr.-KC-in-Hunger-Strike-9-826x495.jpg" alt="new_img" class="img-responsive">
                                          <div class="caption">
                                            <a href="#" class="date-box" role="button"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;१ िदन अगािड</a>
                                            <a href="#" target="_blank"><h4>केसीले आमरण अनसन, उपचार र पढार्इ नरोक्ने (िभिडयोसिहत)</h4></a>
                                            <p class="caption-details">त्रिवि शिक्षण अस्पतालका प्रा. डा. गोविन्द केसीले सोमबार ११ औं आमरण अनसन सुरु गरेका छन् । सरकारले अनसन नबस्न अाग्रह गरे...अस्पतालमा पत्रकार सम्मेलन गरेर ...</p>         
                                          </div>
                                          </div>

                                    </div>
                                
                                   
                                    
                                </div>
                            </div>

                        </div>
                        <!-- row -->

                        <div class="row">
                            <div class="col-sm-3 mega-menu-listing">
                                <a href="#">Stock</a>
                            </div>
                            <div class="col-sm-9 side-bar-menu">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <div class="menu-items">
                                            <ul class="row">
                                                <li class="col-xs-6"><a href="#">Tour</a></li>
                                                <li class="col-xs-6"><a href="#">Tour</a></li>
                                                <li class="col-xs-6"><a href="#">Tour</a></li>
                                                <li class="col-xs-6"><a href="#">Tour</a></li>
                                                <li class="col-xs-6"><a href="#">Tour</a></li>
                                                <li class="col-xs-6"><a href="#">Tour</a></li>
                                                <li class="col-xs-6"><a href="#">Tour</a></li>
                                                <li class="col-xs-6"><a href="#">Tour</a></li>
                                                <li class="col-xs-6"><a href="#">Tour</a></li>
                                                <li class="col-xs-6"><a href="#">Tour</a></li>
                                                <li class="col-xs-6"><a href="#">Tour</a></li>
                                                <li class="col-xs-6"><a href="#">Tour</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="related-menu-image">
                                           
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>

                        </div>
                        <!-- row -->
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="inner-dropdown mega-menu-listing">
                                        <a href="#">Import Export<span class="fa fa-caret-down" aria-hidden="true"></span></a>
                                        <div class="inner-dropdown-menu" role="menu">
                                            <ul>
                                                <li><a href="#">Day Hike</a></li>
                                                <li><a href="#">Day Hike</a></li>
                                                <li><a href="#">Day Hike</a></li>
                                                <li><a href="#">Day Hike</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <!-- row -->


                        <div class="row">
                            <div class="col-sm-3 mega-menu-listing">
                                <a href="#">Budget</a>
                            </div>
                            <div class="col-sm-9 side-bar-menu">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <div class="menu-items">
                                            <ul class="row">
                                                <li class="col-xs-6"><a href="#">Peak Climbing</a></li>
                                                <li class="col-xs-6"><a href="#">Peak Climbing</a></li>
                                                <li class="col-xs-6"><a href="#">Peak Climbing</a></li>
                                                <li class="col-xs-6"><a href="#">Peak Climbing</a></li>
                                                <li class="col-xs-6"><a href="#">Peak Climbing</a></li>
                                                <li class="col-xs-6"><a href="#">Peak Climbing</a></li>
                                                <li class="col-xs-6"><a href="#">Peak Climbing</a></li>
                                                <li class="col-xs-6"><a href="#">Peak Climbing</a></li>
                                                <li class="col-xs-6"><a href="#">Peak Climbing</a></li>
                                                <li class="col-xs-6"><a href="#">Peak Climbing</a></li>
                                                <li class="col-xs-6"><a href="#">Peak Climbing</a></li>
                                                <li class="col-xs-6"><a href="#">Peak Climbing</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="related-menu-image">
                                           
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>

                        </div>


                        <!-- row -->

                        

                        <div class="row">
                            <div class="col-sm-3 mega-menu-listing">
                                <a href="#">Bharkhar Samachar</a>
                            </div>
                            <div class="col-sm-9 side-bar-menu">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <div class="menu-items">
                                            <ul class="row">
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="related-menu-image">
                                           
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>

                        </div>
                        <!-- row -->  


                         <div class="row">
                            <div class="col-sm-3 mega-menu-listing">
                                <a href="#">Bharkhar Samachar</a>
                            </div>
                            <div class="col-sm-9 side-bar-menu">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <div class="menu-items">
                                            <ul class="row">
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="related-menu-image">
                                           
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>

                        </div>
                        <!-- row --> 

                </li>
                <!-- end mega menu -->
                
                <!--mega-menu start-->
                <li class="dropdown mega-dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">मनोरंजन <i class="fa fa-caret-down" aria-hidden="true"></i></a>

                    <div class="dropdown-menu mega-dropdown-menu dropdown-activities">
                        <div class="row">
                            <div class="col-sm-3 mega-menu-listing">
                                <a href="#">विचार</a>
                            </div>
                            <div class="col-sm-9 side-bar-menu first-show">
                                <div class="row">
                                    <h3>Bishesh Samachar</h3>
                                    <div class="col-md-4 ">            
                                            
                                          <div class="thumbnail-box">
                                          <img src="<?php echo $base_url;?>dist/img/Dr.-KC-in-Hunger-Strike-9-826x495.jpg" alt="new_img" class="img-responsive">
                                          <div class="caption">
                                            <a href="#" class="date-box" role="button"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;१ िदन अगािड</a>
                                            <a href="#" target="_blank"><h4>केसीले आमरण अनसन, उपचार र पढार्इ नरोक्ने (िभिडयोसिहत)</h4></a>
                                            <p class="caption-details">त्रिवि शिक्षण अस्पतालका प्रा. डा. गोविन्द केसीले सोमबार ११ औं आमरण अनसन सुरु गरेका छन् । सरकारले अनसन नबस्न अाग्रह गरे...अस्पतालमा पत्रकार सम्मेलन गरेर ...</p>         
                                          </div>
                                          </div>

                                    </div>
                                     <div class="col-md-4 ">            
                                            
                                          <div class="thumbnail-box">
                                          <img src="<?php echo $base_url;?>dist/img/Dr.-KC-in-Hunger-Strike-9-826x495.jpg" alt="new_img" class="img-responsive">
                                          <div class="caption">
                                            <a href="#" class="date-box" role="button"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;१ िदन अगािड</a>
                                            <a href="#" target="_blank"><h4>केसीले आमरण अनसन, उपचार र पढार्इ नरोक्ने (िभिडयोसिहत)</h4></a>
                                            <p class="caption-details">त्रिवि शिक्षण अस्पतालका प्रा. डा. गोविन्द केसीले सोमबार ११ औं आमरण अनसन सुरु गरेका छन् । सरकारले अनसन नबस्न अाग्रह गरे...अस्पतालमा पत्रकार सम्मेलन गरेर ...</p>         
                                          </div>
                                          </div>

                                    </div>
                                     <div class="col-md-4 ">            
                                            
                                          <div class="thumbnail-box">
                                          <img src="<?php echo $base_url;?>dist/img/Dr.-KC-in-Hunger-Strike-9-826x495.jpg" alt="new_img" class="img-responsive">
                                          <div class="caption">
                                            <a href="#" class="date-box" role="button"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;१ िदन अगािड</a>
                                            <a href="#" target="_blank"><h4>केसीले आमरण अनसन, उपचार र पढार्इ नरोक्ने (िभिडयोसिहत)</h4></a>
                                            <p class="caption-details">त्रिवि शिक्षण अस्पतालका प्रा. डा. गोविन्द केसीले सोमबार ११ औं आमरण अनसन सुरु गरेका छन् । सरकारले अनसन नबस्न अाग्रह गरे...अस्पतालमा पत्रकार सम्मेलन गरेर ...</p>         
                                          </div>
                                          </div>

                                    </div>
                                
                                   
                                    
                                </div>
                            </div>

                        </div>
                        <!-- row -->

                        <div class="row">
                            <div class="col-sm-3 mega-menu-listing">
                                <a href="#">Stock</a>
                            </div>
                            <div class="col-sm-9 side-bar-menu">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <div class="menu-items">
                                            <ul class="row">
                                                <li class="col-xs-6"><a href="#">Tour</a></li>
                                                <li class="col-xs-6"><a href="#">Tour</a></li>
                                                <li class="col-xs-6"><a href="#">Tour</a></li>
                                                <li class="col-xs-6"><a href="#">Tour</a></li>
                                                <li class="col-xs-6"><a href="#">Tour</a></li>
                                                <li class="col-xs-6"><a href="#">Tour</a></li>
                                                <li class="col-xs-6"><a href="#">Tour</a></li>
                                                <li class="col-xs-6"><a href="#">Tour</a></li>
                                                <li class="col-xs-6"><a href="#">Tour</a></li>
                                                <li class="col-xs-6"><a href="#">Tour</a></li>
                                                <li class="col-xs-6"><a href="#">Tour</a></li>
                                                <li class="col-xs-6"><a href="#">Tour</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="related-menu-image">
                                           
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>

                        </div>
                        <!-- row -->
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="inner-dropdown mega-menu-listing">
                                        <a href="#">Import Export<span class="fa fa-caret-down" aria-hidden="true"></span></a>
                                        <div class="inner-dropdown-menu" role="menu">
                                            <ul>
                                                <li><a href="#">Day Hike</a></li>
                                                <li><a href="#">Day Hike</a></li>
                                                <li><a href="#">Day Hike</a></li>
                                                <li><a href="#">Day Hike</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <!-- row -->


                        <div class="row">
                            <div class="col-sm-3 mega-menu-listing">
                                <a href="#">Budget</a>
                            </div>
                            <div class="col-sm-9 side-bar-menu">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <div class="menu-items">
                                            <ul class="row">
                                                <li class="col-xs-6"><a href="#">Peak Climbing</a></li>
                                                <li class="col-xs-6"><a href="#">Peak Climbing</a></li>
                                                <li class="col-xs-6"><a href="#">Peak Climbing</a></li>
                                                <li class="col-xs-6"><a href="#">Peak Climbing</a></li>
                                                <li class="col-xs-6"><a href="#">Peak Climbing</a></li>
                                                <li class="col-xs-6"><a href="#">Peak Climbing</a></li>
                                                <li class="col-xs-6"><a href="#">Peak Climbing</a></li>
                                                <li class="col-xs-6"><a href="#">Peak Climbing</a></li>
                                                <li class="col-xs-6"><a href="#">Peak Climbing</a></li>
                                                <li class="col-xs-6"><a href="#">Peak Climbing</a></li>
                                                <li class="col-xs-6"><a href="#">Peak Climbing</a></li>
                                                <li class="col-xs-6"><a href="#">Peak Climbing</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="related-menu-image">
                                           
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>

                        </div>


                        <!-- row -->

                        

                        <div class="row">
                            <div class="col-sm-3 mega-menu-listing">
                                <a href="#">Bharkhar Samachar</a>
                            </div>
                            <div class="col-sm-9 side-bar-menu">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <div class="menu-items">
                                            <ul class="row">
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="related-menu-image">
                                           
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>

                        </div>
                        <!-- row -->  


                         <div class="row">
                            <div class="col-sm-3 mega-menu-listing">
                                <a href="#">Bharkhar Samachar</a>
                            </div>
                            <div class="col-sm-9 side-bar-menu">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <div class="menu-items">
                                            <ul class="row">
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                                <li class="col-xs-6"><a href="#">Adventure</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="related-menu-image">
                                           
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>

                        </div>
                        <!-- row --> 

                </li>
                <!-- end mega menu -->

         
                <li><a href="#view">िवचार</a></li>                       
                <li><a href="#sell">किनमेल  </a></li>
                <li><a href="#sports">खेलकुद  </a></li>                 
                <li><a href="#travel">घुमिफर   </a></li>
                <li><a href="#art"> कला</a></li>
                <li><a href="#blog">ब्लग </a></li>
                <li><a href="#literature">  सािहत्यपाटी  </a></li>
                <li><a href="#pardesh">  प्रदेश न. ६  </a></li>   
                <li><a href="#" class="pull-right">English</a></li>             
             
                
            </ul>
    

        </div>
        <!-- /.nav-collapse -->

    </div>
</nav>

<div class="side-bar-menu">
    <div class="row">
        <div class="col-sm-6"><a href="#">Trekking</a></div>
        <div class="col-sm-6"><a href="#">Trekking</a></div>
        <div class="col-sm-6"><a href="#">Trekking</a></div>
        <div class="col-sm-6"><a href="#">Trekking</a></div>
        <div class="col-sm-6"><a href="#">Trekking</a></div>
        <div class="col-sm-6"><a href="#">Trekking</a></div>
        <div class="col-sm-6"><a href="#">Trekking</a></div>
        <div class="col-sm-6"><a href="#">Trekking</a></div>
        <div class="col-sm-6"><a href="#">Trekking</a></div>
        <div class="col-sm-6"><a href="#">Trekking</a></div>
        <div class="col-sm-6"><a href="#">Trekking</a></div>
    </div>
</div>