<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Angle Khabar</title>
	<!-- <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" /> -->
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Ek+Mukta">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>dist/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>dist/css/owl.carousel.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>dist/css/owl.theme.default.min.css">

	

  <!-- Favicon -->
  <link rel="shortcut icon" type="image/svg" href="<?php echo $base_url;?>dist/img/favicon.png" />
  <link rel="apple-touch-icon" href="<?php echo $base_url;?>dist/img/favicon.png" />
	
  <!-- Jquery -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!--for fallback of cdn-->
  <script>window.jQuery || document.write('<script src="<?php echo $base_url;?>dist/js/jquery.js"><\/script>')</script>

  <script src="<?php echo $base_url;?>dist/js/form-validate.js"></script>

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>

<script src='<?php echo $base_url;?>dist/js/bootstrap.js' ></script>
<script src='<?php echo $base_url;?>dist/js/owl.carousel.min.js' ></script>
<script src='<?php echo $base_url;?>dist/js/jquery.sticky-kit.min.js' ></script>
	<!-- jQuery -->
<script src='<?php echo $base_url;?>dist/js/main.js' ></script> 

</head>
<body>

	<section id="header" class="l-header">

		<div class="header-top">
			<div class="container">
				<div class="row">
					<div class="ht-content ht-mail col-sm-6">
								
								<a href="#" target="_blank" class="col-xs-4">
								<span class="ht-publish">भर्खरै प्रकािशत</span>
								<span class="arrow-right"></span>
								<!-- <i class="fa fa-caret-right fa-2x"></i> -->
								</a>
								<div id="marquee" class="owl-carousel owl-theme ">
								<div class="item">
								<a href="#">&nbsp;सरुवा भएका सचिव मैनालीसँग आयोगले फिर्ता माग्यो गाडी र आइफोन</a></div></div>
					</div>

			        <div class="ht-search col-sm-4  col-sm-offset-2">
			            <form action="" class="search-form">
			                <div class="form-group has-feedback">
			            		<label for="search" class="sr-only">Search</label>
			            		<input type="text" class="form-control" name="search" id="search" placeholder="Search">
			              		<span class="glyphicon glyphicon-search form-control-feedback"></span>
			            	</div>
			            </form>
			        </div>
			    </div>
			</div>
		</div>

		<!--end header-top-->

	<div class="container header-middle">
		<div class="row">
			<div class="col-sm-6">
			<h1 class="logo ">
				<a href="#">
					<img src="<?php echo $base_url;?>dist/img/logo.png" class="logo" alt="logo">
				</a>
			</h1>
			<p class="date"><span >सोमबार, साउन ९, २०७४</span><span>&nbsp;|&nbsp;</span><span >११ : ४१ : ०६</span> </p>
			</div>

			<div class="col-sm-6 advertisement">
				<a href="#" target="_blank">
				<img src="<?php echo $base_url;?>dist/img/add.jpg" class="img-responsive" alt="advertisement">
				</a>
			</div>
		</div>
	</div>




<?php require_once("./include/menu.php") ?>

</section>






	