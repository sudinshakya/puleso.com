<footer id="footer">
<section id="footer-main" class="footer-main">
	<div class="container">
		<div class="col-sm-9">
			<div class="row">
			<div class="col-sm-3 footer-item">
				<ul>
					<li><h3>समाचार</h3></li>
					<li><a href="#">देश </a></li>
					<li><a href="#">िवदेश</a></li>				
				</ul>
			</div>
			

			<div class="col-sm-3 footer-item">
				<ul>
					<li><h3>मनोरंजन </h3></li>
					<li><a href="#">कार्यक्रम  </a></li>
					<li><a href="#">कार्यक्रम </a></li>
					<li><a href="#">कार्यक्रम </a></li>
					<li><a href="#">कार्यक्रम </a></li>				
				</ul>
			</div>
		

			<div class="col-sm-3 footer-item">
				<ul>
					<li><h3>अर्थ</h3></li>
					<li><a href="#">कार्यक्रम  </a></li>
					<li><a href="#">कार्यक्रम </a></li>
					<li><a href="#">कार्यक्रम </a></li>
					<li><a href="#">कार्यक्रम </a></li>			
				</ul>
			</div>
			

			<div class="col-sm-3 footer-item">
				<ul>
					<li><h3>समाचार</h3></li>
					<li><a href="#">कार्यक्रम  </a></li>
					<li><a href="#">कार्यक्रम </a></li>
					<li><a href="#">कार्यक्रम </a></li>
					<li><a href="#">कार्यक्रम </a></li>				
				</ul>
			</div>
			</div>
					

		</div>

		<div class="col-sm-3 footer-item contact">
			<ul>
				<li><h3>सम्पर्क</h3></li>
				<li><a href="#">पुतलीसडक,  काठमाडौँ  नेपाल </a></li>
				<li><a href="mailto: info@angelkhabar.com" data-rel="external"> Gmail : info@angelkhabar.com</a></li>
				<li><a href="tel: +977-9858426707" data-rel="external">Phone : +977-9858426707</a></li>
				
			</ul>
			<br>

			<ul>
				<li><h3>विज्ञापनका लािग</h3></li>
				<li><a href="tel: +977-446021, 9858426707" data-rel="external">मनोज - +977-446021, 9858426707</a></li>
			</ul>		
			
		</div>		
	</div>

	<div class="container logo-bottom">
			<img src="<?php echo $base_url;?>dist/img/logo.png" alt="new_img" class="img-responsive"/>	
		</div>
</section>


<section id="footer-bottom" class="footer-bottom container-fluid">
	<div class="col-sm-12">
		<div class="col-sm-4">
			<p class="copyright">&copy; २०१७ सर्वाधिकार सुरक्षित</p>
		</div>
		<div class="col-sm-4 text-center">
			<ul>
				<li><a href="#"><i class="fa fa-facebook"></i></a></li>
				<li><a href="#"><i class="fa fa-twitter"></i></a></li>
				<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
				<li><a href="#"><i class="fa fa-youtube"></i></a></li>
			</ul>

		</div>
		<div class="col-sm-4">
			<ul>
				<li><a href="#">हाम्ो बारेमा</a></li>
				<li><a href="#">गोपनियेता नीित </a></li>
				<li><a href="#">बिज्ञापन फारम </a></li>
			</ul>
			
		</div>
	</div>
</section>
</footer>
	
	
</body>
</html>