<?php require_once("./include/config.php") ?>
<?php require_once("./include/header.php") ?>
<?php require_once("./include/bread_crumb.php") ?>

<section id="entertainment" class="container" >

	<div class="row">

		<div class="col-sm-5" >
			<div class="card">
				<div class="card-image">
				<a href="#">	
				<img src="http://via.placeholder.com/450x450" class="img-responsive">
				</a>
				 </div>
				 <div class="caption">

				 	<span class="label label-pink">Celebrity</span>
				 	<a href="#">सम्पत्ति माथिल्लो तहले उत्पादन गर्दैन, त्यसले त सोहोर्छ मात्र</a>
				 </div>
			</div>

		</div>

		<div class="col-sm-7">

			<div class="row ent-right">
				<div class="card">
					<div class="card-image2">
						<a href="#">
					 	<img src="<?php echo $base_url;?>dist/img/police-3-825x495.jpg" alt="banner-image" class="img-responsive">
					 	</a>
					</div>
					 <div class="caption">

					 	<span class="label label-pink">Celebrity</span>
					 	<a href="#">सम्पत्ति माथिल्लो तहले उत्पादन गर्दैन, त्यसले त सोहोर्छ मात्र सम्पत्ति माथिल्लो तहले उत्पादन गर्दैन, त्यसले त सोहोर्छ मात्रसम्पत्ति माथिल्लो तहले उत्पादन गर्दैन, त्यसले त सोहोर्छ मात्र</a>
					</div>
				</div>
			   

				<div class="card">
					<div class="card-image2">
						<a href="#">
					 	<img src="<?php echo $base_url;?>dist/img/police-3-825x495.jpg" alt="banner-image" class="img-responsive">
					 	</a>
					</div>

					<div class="caption">
					 	<span class="label label-pink">Celebrity</span>
					 	<a href="#">सम्पत्ति माथिल्लो तहले उत्पादन गर्दैन, त्यसले त सोहोर्छ मात्र</a>
					</div>
				</div>
			

				<div class="card">
					<div class="card-image2">
						<a href="#">
					 	<img src="<?php echo $base_url;?>dist/img/police-3-825x495.jpg" alt="banner-image" class="img-responsive">
					 	</a>
					</div>
					 <div class="caption">

					 	<span class="label label-pink">Celebrity</span>
					 	<a href="#">सम्पत्ति माथिल्लो तहले उत्पादन गर्दैन, त्यसले त सोहोर्छ मात्र</a>
					</div>
				</div>	

				


			</div>
		</div>

	</div>



	<div class="row ">

		<div class="col-sm-3">	

			<div class="right-news advertisement">
			<a href="#">
				<img src="http://via.placeholder.com/300x600" class="add-image">
			</a>
			</div>			

			<div class="right-news business-category ent-category" id="ent-category">
				<h4> &nbsp;&nbsp;Prachalan</h4>
				<hr>
				<ul>
					<li><a href="#">Business</a></li>
					<li><a href="#">Stock Exchange</a></li>
					<li><a href="#">Import/Export</a></li>
					<li><a href="#">Budget</a></li>
					<li><a href="#">More Category More CategoryMore Category</a></li>
					<span class="gradient-right"></span>
				</ul>


			</div>	
		

			

			
			<div class="right-news advertisement" id="sticky-ad" >
				<a href="#" >
				<img src="http://via.placeholder.com/160x600" class="add-image">
				</a>
			</div>

			<div class="right-news advertisement">
				<a href="#">
				<img src="http://via.placeholder.com/350x650" class="add-image">
				</a>
			</div>

			<div class="right-news advertisement">
				<a href="#">
				<img src="http://via.placeholder.com/350x650" class="add-image">
				</a>
			</div>

			<div class="right-news advertisement">
				<a href="#">
				<img src="http://via.placeholder.com/350x650" class="add-image">
				</a>
			</div>
			

		</div>


		<div class="col-sm-9">			

			<div class="row left-featured ">			
					
					<div class="thumbnail-box">			
						
						  <a href="#">
					      <img src="<?php echo $base_url;?>dist/img/Dr.-KC-in-Hunger-Strike-9-826x495.jpg" alt="new_img" class="img-responsive">
					      </a>
					      <div class="caption">
					        <a href="#" target="_blank"><h4>केसीले आमरण अनसन, उपचार र पढार्इ नरोक्ने (िभिडयोसिहत)</h4></a>
					        <p class="caption-details">त्रिवि शिक्षण अस्पतालका प्रा. डा. गोविन्द केसीले सोमबार ११ औं आमरण अनसन सुरु गरेका छन् । सरकारले अनसन नबस्न अाग्रह गरे...अस्पतालमा पत्रकार सम्मेलन गरेर ...</p>	       
					      </div>

					</div>

					<div class=" thumbnail-box">			
						 <a href="#">
					      <img src="<?php echo $base_url;?>dist/img/Dr.-KC-in-Hunger-Strike-9-826x495.jpg" alt="new_img" class="img-responsive">
					      </a>
					      <div class="caption">
					        <a href="#" target="_blank"><h4>केसीले आमरण अनसन, उपचार र पढार्इ नरोक्ने (िभिडयोसिहत)</h4></a>
					        <p class="caption-details">त्रिवि शिक्षण अस्पतालका प्रा. डा. गोविन्द केसीले सोमबार ११ औं आमरण अनसन सुरु गरेका छन् । सरकारले अनसन नबस्न अाग्रह गरे...अस्पतालमा पत्रकार सम्मेलन गरेर ...</p>	       
					      </div>

					</div>

					<div class=" thumbnail-box ">			
						<a href="#">
					      <img src="http://via.placeholder.com/300x600" class="img-responsive">
					      </a>
					      <div class="caption">
					        <a href="#" target="_blank"><h4>केसीले आमरण अनसन, उपचार र पढार्इ नरोक्ने (िभिडयोसिहत)</h4></a>
					        <p class="caption-details">त्रिवि शिक्षण अस्पतालका प्रा. डा. गोविन्द केसीले सोमबार ११ औं आमरण अनसन सुरु गरेका छन् । सरकारले अनसन नबस्न अाग्रह गरे...अस्पतालमा पत्रकार सम्मेलन गरेर ...</p>	       
					      </div>

					</div>

					<div class=" thumbnail-box">			
						<a href="#">
					      <img src="<?php echo $base_url;?>dist/img/Dr.-KC-in-Hunger-Strike-9-826x495.jpg" alt="new_img" class="img-responsive">
					      </a>
					      <div class="caption">
					        <a href="#" target="_blank"><h4>केसीले आमरण अनसन, उपचार र पढार्इ नरोक्ने (िभिडयोसिहत)</h4></a>
					        <p class="caption-details">त्रिवि शिक्षण अस्पतालका प्रा. डा. गोविन्द केसीले सोमबार ११ औं आमरण अनसन सुरु गरेका छन् । सरकारले अनसन नबस्न अाग्रह गरे...अस्पतालमा पत्रकार सम्मेलन गरेर ...</p>	       
					      </div>

					</div>
				
			
			</div>

			<div class="row">
				<a href="#">
				 <img src="<?php echo $base_url;?>dist/img/advertisement3.png" alt="new_img" class="advertisement3">
				 </a>
			
			</div>

			<div class="row">
				<div class="col-sm-12 ">
					<div class="row ent-list bg-white">
							<div class="col-xs-6 ">
							<a href="#">
							  <img src="<?php echo $base_url;?>dist/img/IMG_7102-826x495.jpg" alt="new_img" class="img-responsive">	
							  </a>					      
						    </div>

						    <div class="col-xs-6">
						      	<p  class="date-box"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;१ िदन अगािड</p>
						      	
						        <a href="#" target="_blank"> <h4>१६ वर्षे सन्दीपको क्रिकेट उडान</h4></a>

						        <p>दुई वर्षअघि युथ–१९ विश्व कप छनोटका लागि काठमाडौंको यात्रा तय गर्दा १४ वर्षीय सन्दीप लामिछानेको मनमा अनगिन्ति प्रश्न थिए। कम उमेर भएको कारणले गर्दा होला ....</p>	      
						      </div>
						      <div class="clearfix"></div>
					</div>	

					<div class="row ent-list bg-white">
							<div class="col-xs-6 ">
							<a href="#">
							  <img src="<?php echo $base_url;?>dist/img/IMG_7102-826x495.jpg" alt="new_img" class="img-responsive">	
							  </a>					      
						    </div>

						    <div class="col-xs-6">
						      	<p  class="date-box"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;१ िदन अगािड</p>
						      	
						        <a href="#" target="_blank"> <h4>१६ वर्षे सन्दीपको क्रिकेट उडान</h4></a>

						        <p>दुई वर्षअघि युथ–१९ विश्व कप छनोटका लागि काठमाडौंको यात्रा तय गर्दा १४ वर्षीय सन्दीप लामिछानेको मनमा अनगिन्ति प्रश्न थिए। कम उमेर भएको कारणले गर्दा होला ....</p>	      
						      </div>
						      <div class="clearfix"></div>
					</div>	

					<div class="row ent-list bg-white">
							<div class="col-xs-6 ">
							<a href="#">
							  <img src="<?php echo $base_url;?>dist/img/IMG_7102-826x495.jpg" alt="new_img" class="img-responsive">		
							  </a>				      
						    </div>

						    <div class="col-xs-6">
						      	<p  class="date-box"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;१ िदन अगािड</p>
						      	
						        <a href="#" target="_blank"> <h4>१६ वर्षे सन्दीपको क्रिकेट उडान</h4></a>

						        <p>दुई वर्षअघि युथ–१९ विश्व कप छनोटका लागि काठमाडौंको यात्रा तय गर्दा १४ वर्षीय सन्दीप लामिछानेको मनमा अनगिन्ति प्रश्न थिए। कम उमेर भएको कारणले गर्दा होला ....</p>	      
						      </div>
						      <div class="clearfix"></div>
					</div>					
				</div>			
			
			</div>

			<div class="row">
				<a href="#">
				 <img src="<?php echo $base_url;?>dist/img/advertisement3.png" alt="new_img" class="advertisement3">
				 </a>
			
			</div>

			<div class="row left-featured bg-white entertainment">
				<div class="col-sm-12 featured-header">
					<h3 class="col-xs-6 ">मनोरंजन</h3>
					<div class="col-xs-6 ">
						<a class="btn-direction customPrevBtnFour" href="javascript:void(null);"><i class="fa fa-chevron-right"></i></a>
						<a class="btn-direction customNextBtnFour" href="javascript:void(null);"><i class="fa fa-chevron-left"></i></a>
					
					 </div>
				 </div>
				 <div class="col-xs-12   owl-carousel owl-theme" id="owl-four">				
					<div class="thumbnail item">
						<a href="">
					      <img src="<?php echo $base_url;?>dist/img/neeta-nani-180x280.jpg" alt="new_img" class="img-responsive">
					      </a>
					      <div class="caption">
					    
					        <a  href="#" target="_blank"><h4>केसीले आमरण अनसन, उपचार र पढार्इ नरोक्ने (िभिडयोसिहत)</h4></a>
					              
					      </div>
					</div>

					<div class="thumbnail item">
						<a href="">
					      <img src="<?php echo $base_url;?>dist/img/neeta-nani-180x280.jpg" alt="new_img" class="img-responsive">
					      </a>
					      <div class="caption">
					    
					        <a  href="#" target="_blank"><h4>केसीले आमरण अनसन, उपचार र पढार्इ नरोक्ने (िभिडयोसिहत)</h4></a>
					              
					      </div>
					</div>

					<div class="thumbnail item">
						<a href="">
					      <img src="<?php echo $base_url;?>dist/img/neeta-nani-180x280.jpg" alt="new_img" class="img-responsive">
					      </a>
					      <div class="caption">
					    
					        <a  href="#" target="_blank"><h4>केसीले आमरण अनसन, उपचार र पढार्इ नरोक्ने (िभिडयोसिहत)</h4></a>
					              
					      </div>
					</div>



					
				 </div>
			
			</div>

			
		</div>		

		

		
	</div>





</section>



<?php require_once("./include/footer.php") ?>