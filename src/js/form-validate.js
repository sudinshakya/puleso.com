// Wait for the DOM to be ready
$(function() {

  jQuery.validator.addMethod("lettersonly", function(value, element){
   return this.optional(element) || /^[a-z ]+$/i.test(value);
  }, "Letters and spaces only please");

  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("#request-form").validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      fullName: {
        required: true,
        lettersonly: true,
        minlength: 2
      },
      phone:{required: true,        
        minlength: 2
      },
      email: {
        required: true,
        // Specify that email should be validated
        // by the built-in "email" rule
        email: true
      }, 
      city: {
        required: true
      },
      help:{
        required: true,
        lettersonly: true,
        minlength: 2
      }, 

    },
    // Specify validation error messages
    messages: {
      fullName: {
        required: "This field is required.",
        minlength: "Fullname too short"
      },
      phone: {
        required: "This field is required.",
        digits:"Please enter valid number",
        minlength: "Enter your contact number"
      },
      email: {
        required: "This field is required.",
        email: "Email not valid"
      },
      city: {
        required:"Select a city!"
      },
      help:{
        required:"Please enter the help message!",
        minlength:"Please enter valid message"
      } 
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });
});