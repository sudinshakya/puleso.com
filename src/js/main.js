$(document).ready(function() {

    var fxHeader = $('.l-header');
        var headerHeight = $('.l-header').outerHeight(true);
        $(window).scroll(function () {
            scroll = $(window).scrollTop();
            if(scroll >= headerHeight){
                fxHeader.addClass('fixed')
            }
            else{
                fxHeader.removeClass('fixed')
            }
        });


 
  //for smooth scrolling
  // $('a[href*="#"]:not([href="#"])').click(function() {
  //   if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
  //     var target = $(this.hash);
  //     target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
  //     if (target.length) {
  //       $('html, body').animate({
  //         scrollTop: target.offset().top
  //       }, 1000);
  //       return false;
  //     }
  //   }
  // });

//   $(document).ready(function() {
//     $('a[href*=#]').bind('click', function(e) {
//         e.preventDefault(); // prevent hard jump, the default behavior

//         var target = $(this).attr("href"); // Set the target as variable

//         // perform animated scrolling by getting top-position of target-element and set it as scroll target
//         $('html, body').stop().animate({
//             scrollTop: $(target).offset().top
//         }, 600, function() {
//             location.hash = target; //attach the hash (#jumptarget) to the pageurl
//         });

//         return false;
//     });
// });








});

// For owlcarousel entertainment
$(document).ready(function(){
  $("#owl-one").owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    responsiveClass:true,
    margin:10,
    autoplay:true,
    autoplayTimeout:1000,
    autoplayHoverPause:true,
    responsive:{
        0:{
            items:1
            
        },
        600:{
            items:2
            
        },
        1000:{
            items:3
          
        }
    }
   
  });

    var owl = $('#owl-one');
    owl.owlCarousel();
    // Go to the next item
    $('.customNextBtn').click(function() {
        owl.trigger('next.owl.carousel');
    })
    // Go to the previous item
    $('.customPrevBtn').click(function() {
        // With optional speed parameter
        // Parameters has to be in square bracket '[]'
        owl.trigger('prev.owl.carousel', [300]);
    })
});

// For owlcarousel horoscope
$(document).ready(function(){
  $("#owl-two").owlCarousel({
      navigation : true, // Show next and prev buttons
      loop:true,
      // slideSpeed : 300,
      // paginationSpeed : 400,
      autoplay:true,
      autoplayTimeout:1000,
      autoplayHoverPause:true,
      
      items : 1, 
      itemsDesktop : false,
      itemsDesktopSmall : false,
      itemsTablet: false,
      itemsMobile : false
  });

    var owl = $('#owl-two');
    owl.owlCarousel();
    // Go to the next item
    $('.customNextBtnTwo').click(function() {
        owl.trigger('next.owl.carousel');
    })
    // Go to the previous item
    $('.customPrevBtnTwo').click(function() {
        // With optional speed parameter
        // Parameters has to be in square bracket '[]'
        owl.trigger('prev.owl.carousel', [300]);
    })
});

// For owlcarousel marquee

$(document).ready(function(){
  $("#marquee").owlCarousel({
      navigation : true, // Show next and prev buttons
      loop:true,
      // slideSpeed : 300,
      // paginationSpeed : 400,
      autoplay:true,
      autoplayTimeout:1000,
      autoplayHoverPause:true,
      
      items : 1, 
      itemsDesktop : false,
      itemsDesktopSmall : false,
      itemsTablet: false,
      itemsMobile : false
  });


   
});

// For owlcarousel entertainment
$(document).ready(function(){
  $("#owl-four").owlCarousel({
    loop:true,
    items:4,
    margin:10,
    nav:true,
    responsiveClass:true,
    margin:10,
    autoplay:true,
    autoplayTimeout:1000,
    autoplayHoverPause:true,
    // responsive:{
    //     0:{
    //         items:1
            
    //     },
    //     600:{
    //         items:2
            
    //     },
    //     1000:{
    //         items:3
          
    //     }
    // }
   
  });

    var owl = $('#owl-four');
    owl.owlCarousel();
    // Go to the next item
    $('.customNextBtnFour').click(function() {
        owl.trigger('next.owl.carousel');
    })
    // Go to the previous item
    $('.customPrevBtnFour').click(function() {
        // With optional speed parameter
        // Parameters has to be in square bracket '[]'
        owl.trigger('prev.owl.carousel', [300]);
    })
});




//mega drop down

$(document).ready(function(){

// jQuery(document).on('click', '.mega-dropdown', function(e) {
//   e.stopPropagation()
// })

 //mega menu js
  jQuery(document).on('click', '.mega-dropdown', function(e) {
  e.stopPropagation()
})
$(".inner-dropdown a").click(function(){
    $(".inner-dropdown-menu").slideToggle(500);
}); 


  $(".mega-menu-listing").click(function(){
    var div = $(this).next(".side-bar-menu");
    $(".side-bar-menu").not(div).slideUp("slow");
    div.slideToggle("slow");
     //$(this).toggleClass("open-submenu");
  });

  

 if ($(window).width() > 991) {
  $(document).ready(function(){
     $(".first-show").show();
  });
}
});



//for fixed addvertisement in entertainment page
$(document).ready(function(){
  $("#sticky-ad").stick_in_parent();
});



