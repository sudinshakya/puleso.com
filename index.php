<?php require_once("./include/config.php") ?>
<?php require_once("./include/header.php") ?>

<section id="main" class="container">

	<div class="row ">

		<div class="col-sm-12 hero">
			<h1><a href="#" target="_blank">सम्पत्ति माथिल्लो तहले उत्पादन गर्दैन, त्यसले त सोहोर्छ मात्र</a></h1>
			<div class="hero-image text-center">
				<a href="#">
				 <img src="<?php echo $base_url;?>dist/img/police-3-825x495.jpg" alt="banner-image" class="img-responsive">
				 </a>
			</div>
			<p class="hero-details">-रूड्गर ब्रेग्मेन<br>
			संसारको सबैभन्दा ठूलो ट्याक्सी कम्पनी यूबरको आफ्नो ट्याक्सी एउटै पनि छैन, सबैभन्दा प्रख्यात मिडिया मालिक फेसबुक आफैंले एउटै विषयवस्तु सिर्जना गर्दैन, संसारको सबैभन्दा नामी खुद्रा व्यापारी अलिबाबाको आफ्नो  मौज्दात नै छैन, संसारको सबैभन्दा ठूलो आवास कम्पनी एअरबिएनबिको आफ्नो आवास क्षेत्र छैन ।</p>

		</div>

	</div>


	<div class="row">
		<div class="col-sm-9">
			<div class="row left-featured bg-white">

			<div class="col-md-8">
				<div id="bannerCarousel" class="carousel slide" data-ride="carousel">
				  <!-- Indicators -->
				  <ol class="carousel-indicators">
				    <li data-target="#bannerCarousel" data-slide-to="0" class="active"></li>
				    <li data-target="#bannerCarousel" data-slide-to="1"></li>
				    <li data-target="#bannerCarousel" data-slide-to="2"></li>
				  </ol>

				  <!-- Wrapper for slides -->
				  <div class="carousel-inner">
				    <div class="item active">
				      <img src="<?php echo $base_url;?>dist/img/police-3-825x495.jpg" alt="Chania">
				      <div class="carousel-caption">
				        <a href="#" target="_blank"><h3  >पोिलस ब्राण्डको शोरुम दरवारमार्गमा</h3></a>
						
				      </div>
				    </div>

				    <div class="item">
				      <img src="<?php echo $base_url;?>dist/img/police-3-825x495.jpg" alt="Chicago">
				      <div class="carousel-caption">
				       <a href="#" target="_blank"> <h3  >पोिलस ब्राण्डको शोरुम दरवारमार्गमा</h3></a>
					
				      </div>
				    </div>

				    <div class="item">
				      <img src="<?php echo $base_url;?>dist/img/police-3-825x495.jpg" alt="New York">
				      <div class="carousel-caption">
				        <a href="#" target="_blank"><h3  >पोिलस ब्राण्डको शोरुम दरवारमार्गमा</h3></a>
						
				      </div>
				    </div>
				  </div>

				  <!-- Left and right controls -->
				  <a class="left carousel-control" href="#bannerCarousel" data-slide="prev">
				    <span class="glyphicon glyphicon-chevron-left"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="right carousel-control" href="#bannerCarousel" data-slide="next">
				    <span class="glyphicon glyphicon-chevron-right"></span>
				    <span class="sr-only">Next</span>
				  </a>
				</div>

			</div><!--ed col-sm-8-->

			<!-- 	<div class="col-sm-8 ">	
					<div class="hero">
					<div class="hero-contents">
						<p class="hero-header">राजनीित</p>
					<h3 class="hero-title" >पोिलस ब्राण्डको शोरुम दरवारमार्गमा</h3>
					<p class="hero-details">विश्व प्रसिद्ध फेसन ब्राण्ड पोलिसका समाग्रीहरुको आधिकारिक बिक्री केन्द्र दरवार मार्गमा खुलेको छ। शोरुमको नेपाल उद्योग बाणिज्य महासंघका अध्यक्ष भवानी राणाले शुभारम्भ गरिन् विश्वको अग्रणी।</p>
					</div>
					</div>
				</div> -->

				<div class="col-md-4 thumbnail-box">			
					
					      <img src="<?php echo $base_url;?>dist/img/Dr.-KC-in-Hunger-Strike-9-826x495.jpg" alt="new_img" class="img-responsive">
					      <div class="caption">
					      	<a href="#" class="date-box" role="button"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;१ िदन अगािड</a>
					        <a href="#" target="_blank"><h4>केसीले आमरण अनसन, उपचार र पढार्इ नरोक्ने (िभिडयोसिहत)</h4></a>
					        <p class="caption-details">त्रिवि शिक्षण अस्पतालका प्रा. डा. गोविन्द केसीले सोमबार ११ औं आमरण अनसन सुरु गरेका छन् । सरकारले अनसन नबस्न अाग्रह गरे...अस्पतालमा पत्रकार सम्मेलन गरेर ...</p>	       
					      </div>

				</div>

			</div>

			<div class="row left-featured bg-white">
				<div class="col-sm-12 featured-header">
						<h3 class="col-xs-6 ">राजनीित</h3>
						
			    </div>

				<div class="col-sm-8 latest-left">	
				
						<div class="sports thumbnail-box">
							<div class="sports-image">
							  <img src="<?php echo $base_url;?>dist/img/IMG_7102-826x495.jpg" alt="new_img" class="img-responsive">						      
						      </div>

						      <div class="sports-details">
						      	<a href="#" class="date-box" role="button"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;१ िदन अगािड</a>
						      	
						        <a href="#" target="_blank"> <h4>१६ वर्षे सन्दीपको क्रिकेट उडान</h4></a>

						        <p>दुई वर्षअघि युथ–१९ विश्व कप छनोटका लागि काठमाडौंको यात्रा तय गर्दा १४ वर्षीय सन्दीप लामिछानेको मनमा अनगिन्ति प्रश्न थिए। कम उमेर भएको कारणले गर्दा होला ....</p>	      
						      </div>
						      <div class="clearfix"></div>
						</div>

						<div class="sports thumbnail-box">
							<div class="sports-image ">
							  <img src="<?php echo $base_url;?>dist/img/IMG_7102-826x495.jpg" alt="new_img" class="img-responsive">						      
						      </div>

						      <div class="sports-details">
						      	<a href="#" class="date-box" role="button"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;१ िदन अगािड</a>
						      	
						        <a href="#" target="_blank"> <h4>१६ वर्षे सन्दीपको क्रिकेट उडान</h4></a>

						        <p>दुई वर्षअघि युथ–१९ विश्व कप छनोटका लागि काठमाडौंको यात्रा तय गर्दा १४ वर्षीय सन्दीप लामिछानेको मनमा अनगिन्ति प्रश्न थिए। कम उमेर भएको कारणले गर्दा होला ....</p>	      
						      </div>
						      <div class="clearfix"></div>
						</div>

						
					
				</div>

				<div class="col-sm-4 latest-right thumbnail-box">			
					
					      <img src="<?php echo $base_url;?>dist/img/Dr.-KC-in-Hunger-Strike-9-826x495.jpg" alt="new_img" class="img-responsive">
					      <div class="caption">
					      	<a href="#" class="date-box" role="button"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;१ िदन अगािड</a>
					        <a href="#" target="_blank"><h4>केसीले आमरण अनसन, उपचार र पढार्इ नरोक्ने (िभिडयोसिहत)</h4></a>
					        <p>त्रिवि शिक्षण अस्पतालका प्रा. डा. गोविन्द केसीले सोमबार ११ औं आमरण अनसन सुरु गरेका छन् । सरकारले अनसन नबस्न अाग्रह गरे...अस्पतालमा पत्रकार सम्मेलन गरेर ...</p>	       
					      </div>

				</div>
				
			</div>

			<div class="row">
				
				 <img src="<?php echo $base_url;?>dist/img/advertisement3.png" alt="new_img" class="advertisement3">
			
			</div>	

			<div class="row left-featured bg-white">
				<div class="col-sm-12 featured-header">
					<h3 class="col-xs-6 ">राजनीित</h3>
					<div class="col-xs-6 ">
						<a class="btn-green pull-right" href="#">सबै</a>	
					 </div>
				 </div>
				 <div class="">
					 <div class="politics-left thumbnail-box">				
					
						<img src="<?php echo $base_url;?>dist/img/Sher-bahadur-Dauba.jpg" alt="politics-img">					      
				       <div class="caption">
						<br>
				        <a href="#" target="_blank"><h4>मन्त्रिपरिषद् विस्तार बुधबार, कांग्रेसले मन्त्रीको  नाम टुंग्या</h4></a>

				      	<a href="#" class="date-box" role="button"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;१ िदन अगािड</a>
				       
				        <p>त्रिवि शिक्षण अस्पतालका प्रा. डा. गोविन्द केसीले सोमबार ११ औं आमरण अनसन सुरु गरेका छन् । सरकारले अनसन नबस्न अाग्रह गरे...अस्पतालमा पत्रकार सम्मेलन गरेर ...</p>	   

				      </div> 
					 </div>

					<div class="politics-right">

						<div class="politics-list">
				
						 	<img src="<?php echo $base_url;?>dist/img/prachanda-and-kamal-thapa.jpg" alt="politics-img">
					
						 	<div class="politics-details">
						 		<a href="#" target="_blank"><h4>प्रचण्डले कमल थापालाई फोन गर्छु  भनेर ढाँटे</h4></a>
						 		<p>सत्तारुढ दलले आफूहरुलाई अपमानित गरेको भन्दै राप्रपाका नेताहरुले सरकारमा नजाने चेतावनी दिएका छन् ।हुनमन्त्रषदमा</p>
						 	</div>

						</div>

						<div class="politics-list">
				
						 	<img src="<?php echo $base_url;?>dist/img/prachanda-and-kamal-thapa.jpg" alt="politics-img">
					
						 	<div class="politics-details">
						 		<a href="#" target="_blank"><h4>प्रचण्डले कमल थापालाई फोन गर्छु  भनेर ढाँटे</h4></a>
						 		<p>सत्तारुढ दलले आफूहरुलाई अपमानित गरेको भन्दै राप्रपाका नेताहरुले सरकारमा नजाने चेतावनी दिएका छन् ।हुनमन्त्रषदमा</p>
						 	</div>
					 	
						</div>

						<div class="politics-list">
				
						 	<img src="<?php echo $base_url;?>dist/img/prachanda-and-kamal-thapa.jpg" alt="politics-img">
					
						 	<div class="politics-details">
						 		<a href="#" target="_blank"><h4>प्रचण्डले कमल थापालाई फोन गर्छु  भनेर ढाँटे</h4></a>
						 		<p>सत्तारुढ दलले आफूहरुलाई अपमानित गरेको भन्दै राप्रपाका नेताहरुले सरकारमा नजाने चेतावनी दिएका छन् ।हुनमन्त्रषदमा</p>
						 	</div>
					 	
						</div>

					</div>
				 </div>
			
			</div>

			<div class="row left-featured bg-white entertainment">
				<div class="col-sm-12 featured-header">
					<h3 class="col-xs-6 ">मनोरंजन</h3>
					<div class="col-xs-6 ">
						<a class="btn-direction customPrevBtn" href="javascript:void(null);"><i class="fa fa-chevron-right"></i></a>
						<a class="btn-direction customNextBtn" href="javascript:void(null);"><i class="fa fa-chevron-left"></i></a>
					
					 </div>
				 </div>
				 <div class="col-xs-12   owl-carousel owl-theme" id="owl-one">				
					<div class="thumbnail item">
						  <a href="#">	
					      <img src="<?php echo $base_url;?>dist/img/ok-2-30062017122337-0x825.jpg" alt="new_img" class="img-responsive">
					      </a>
					      <div class="caption">
					      	<a href="#" class="date-box" role="button"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;१ िदन अगािड</a>
					        <a  href="#" target="_blank"><h4>केसीले आमरण अनसन, उपचार र पढार्इ नरोक्ने (िभिडयोसिहत)</h4></a>
					        <p>त्रिवि शिक्षण अस्पतालका प्रा. डा. गोविन्द केसीले सोमबार ११ औं आमरण अनसन सुरु गरेका छन् । सरकारले अनसन नबस्न अाग्रह गरे...अस्पतालमा पत्रकार सम्मेलन गरेर ...</p>	       
					      </div>
					</div>

					<div class=" thumbnail item">
						<a href="#">
					      <img src="<?php echo $base_url;?>dist/img/ok-2-30062017122337-0x825.jpg" alt="new_img" class="img-responsive">
					      </a>
					      <div class="caption">
					      	<a href="#" class="date-box" role="button"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;१ िदन अगािड</a>
					        <a  href="#" target="_blank"> <h4>केसीले आमरण अनसन, उपचार र पढार्इ नरोक्ने (िभिडयोसिहत)</h4></a>
					        <p>त्रिवि शिक्षण अस्पतालका प्रा. डा. गोविन्द केसीले सोमबार ११ औं आमरण अनसन सुरु गरेका छन् । सरकारले अनसन नबस्न अाग्रह गरे...अस्पतालमा पत्रकार सम्मेलन गरेर ...</p>	       
					      </div>
					</div>

					<div class=" thumbnail item">
						<a href="#">
					      <img src="<?php echo $base_url;?>dist/img/ok-2-30062017122337-0x825.jpg" alt="new_img" class="img-responsive">
					      </a>
					      <div class="caption">
					      	<a href="#" class="date-box" role="button"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;१ िदन अगािड</a>
					         <a  href="#" target="_blank"><h4>केसीले आमरण अनसन, उपचार र पढार्इ नरोक्ने (िभिडयोसिहत)</h4></a>
					        <p>त्रिवि शिक्षण अस्पतालका प्रा. डा. गोविन्द केसीले सोमबार ११ औं आमरण अनसन सुरु गरेका छन् । सरकारले अनसन नबस्न अाग्रह गरे...अस्पतालमा पत्रकार सम्मेलन गरेर ...</p>	       
					      </div>
					</div>
					
				 </div>
			
			</div>
		</div>		

		<div class="col-sm-3">	

			<div class="advertisement2">
				<a href="#">
				<img src="<?php echo $base_url;?>dist/img/hundai-h.png" alt="new_img" class="img-responsive"/>	
				</a>

			</div>

			<div class="right-news">

			<div>

			  <!-- Nav tabs -->
			  <ul class="nav nav-tabs right-title" role="tablist">
			    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">मुख्य खबर</a></li>
			    <li>|</li>
			    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">लोकप्रिय</a></li>
			    
			  </ul>

			  <!-- Tab panes -->
			  <div class="tab-content">
			    <div role="tabpanel" class="tab-pane active" id="home">
					<div class="news-list">

					<div class="list-img">
					<img src="<?php echo $base_url;?>dist/img/army-chif-meet-50x50.jpg" alt="new_img" />
					</div>
					<div class="list-detail">
					<a href="#">प्रधानसेनापति र भारतीय आसाम राइफल्सका डाइरेक्टर जनरलबीच भेटवा</a>
					</div>
					<div class="clearfix"></div>

				</div>		

					<div class="news-list">

						<div class="list-img">
						<img src="<?php echo $base_url;?>dist/img/army-chif-meet-50x50.jpg" alt="new_img" />
						</div>
						<div class="list-detail">
						<a href="#">प्रधानसेनापति र भारतीय आसाम राइफल्सका डाइरेक्टर जनरलबीच भेटवा</a>
						</div>
						<div class="clearfix"></div>

					</div>

					<div class="news-list">

						<div class="list-img">
						<img src="<?php echo $base_url;?>dist/img/army-chif-meet-50x50.jpg" alt="new_img" />
						</div>
						<div class="list-detail">
						<a href="#">प्रधानसेनापति र भारतीय आसाम राइफल्सका डाइरेक्टर जनरलबीच भेटवा</a>
						</div>
						<div class="clearfix"></div>

					</div>

					<div class="news-list">

						<div class="list-img">
						<img src="<?php echo $base_url;?>dist/img/army-chif-meet-50x50.jpg" alt="new_img" />
						</div>
						<div class="list-detail">
						<a href="#">प्रधानसेनापति र भारतीय आसाम राइफल्सका डाइरेक्टर जनरलबीच भेटवा</a>
						</div>
						<div class="clearfix"></div>

					</div>

					<div class="news-list">

						<div class="list-img">
						<img src="<?php echo $base_url;?>dist/img/army-chif-meet-50x50.jpg" alt="new_img" />
						</div>
						<div class="list-detail">
						<a href="#">प्रधानसेनापति र भारतीय आसाम राइफल्सका डाइरेक्टर जनरलबीच भेटवा</a>
						</div>
						<div class="clearfix"></div>
					</div>	

					<div class="news-list">

						<div class="list-img">
						<img src="<?php echo $base_url;?>dist/img/army-chif-meet-50x50.jpg" alt="new_img" />
						</div>
						<div class="list-detail">
						<a href="#">प्रधानसेनापति र भारतीय आसाम राइफल्सका डाइरेक्टर जनरलबीच भेटवा</a>
						</div>
						<div class="clearfix"></div>

					</div>						
			    </div>
			    <div role="tabpanel" class="tab-pane" id="profile">

			    	<div class="news-list">

						<div class="list-img">
						<img src="<?php echo $base_url;?>dist/img/army-chif-meet-50x50.jpg" alt="new_img" />
						</div>
						<div class="list-detail">
						<a href="#">प्रधानसेनापति  प्रधानसेनापति प्रधानसेनापतिर  राइफल्सका डाइरेक्टर जनरलबीच भेटवा</a>
						</div>
						<div class="clearfix"></div>

					</div>		

					<div class="news-list">

						<div class="list-img">
						<img src="<?php echo $base_url;?>dist/img/army-chif-meet-50x50.jpg" alt="new_img" />
						</div>
						<div class="list-detail">
						<a href="#">प्रधानसेनापति र भारतीय आसाम राइफल्सका डाइरेक्टर जनरलबीच भेटवा</a>
						</div>
						<div class="clearfix"></div>

					</div>

					<div class="news-list">

						<div class="list-img">
						<img src="<?php echo $base_url;?>dist/img/army-chif-meet-50x50.jpg" alt="new_img" />
						</div>
						<div class="list-detail">
						<a href="#">प्रधानसेनापति र भारतीय आसाम राइफल्सका डाइरेक्टर जनरलबीच भेटवा</a>
						</div>
						<div class="clearfix"></div>

					</div>

					<div class="news-list">

						<div class="list-img">
						<img src="<?php echo $base_url;?>dist/img/army-chif-meet-50x50.jpg" alt="new_img" />
						</div>
						<div class="list-detail">
						<a href="#">प्रधानसेनापति र भारतीय आसाम राइफल्सका डाइरेक्टर जनरलबीच भेटवा</a>
						</div>
						<div class="clearfix"></div>

					</div>

					<div class="news-list">

						<div class="list-img">
						<img src="<?php echo $base_url;?>dist/img/army-chif-meet-50x50.jpg" alt="new_img" />
						</div>
						<div class="list-detail">
						<a href="#">प्रधानसेनापति र भारतीय आसाम राइफल्सका डाइरेक्टर जनरलबीच भेटवा</a>
						</div>
						<div class="clearfix"></div>
					</div>	

					<div class="news-list">

						<div class="list-img">
						<img src="<?php echo $base_url;?>dist/img/army-chif-meet-50x50.jpg" alt="new_img" />
						</div>
						<div class="list-detail">
						<a href="#">प्रधानसेनापति र भारतीय आसाम राइफल्सका डाइरेक्टर जनरलबीच भेटवा</a>
						</div>
						<div class="clearfix"></div>

					</div>						
			    </div>		    
			  </div>

			</div>
		
			</div>

			<div class="right-news">

				<h3 class="right-title">आजको राशिफल&nbsp;&nbsp;&nbsp;<a href="avascript:void(null);" class="btn-direction customPrevBtnTwo"><i class="fa fa-chevron-left"></i></a>
				<a href="javascript:void(null);" class="btn-direction customNextBtnTwo"><i class="fa fa-chevron-right"></i></a></h3>
			
				<hr>
				<div class="owl-carousel owl-theme" id="owl-two">				
				
					<div class="horoscope-container item ">


						<div class="horoscope">

							<div class="horoscope-img">
								<img src="<?php echo $base_url;?>dist/img/leo.png" alt="leo" />
							</div>
							<div class="horoscope-detail">
								<h3>मेष</h3>
								<p>तपाई को आजको राशिफल एस प्रकार छ तपाई को आजको राशिफल एस प्रकार छ तपाई को आजको राशिफल एस प्रकार छ</p>
							</div>
							<div class="clearfix"></div>

						</div>

						<div class="horoscope">

							<div class="horoscope-img">
								<img src="<?php echo $base_url;?>dist/img/leo.png" alt="leo" />
							</div>
							<div class="horoscope-detail">
								<h3>मेष</h3>
								<p >तपाई को आजको राशिफल एस प्रकार छ तपाई को आजको राशिफल एस प्रकार छ तपाई को आजको राशिफल एस प्रकार छ</p>
							</div>
							<div class="clearfix"></div>

						</div>

						<div class="horoscope">

							<div class="horoscope-img">
								<img src="<?php echo $base_url;?>dist/img/leo.png" alt="leo" />
							</div>
							<div class="horoscope-detail">
								<h3>मेष</h3>
								<p >तपाई को आजको राशिफल एस प्रकार छ तपाई को आजको राशिफल एस प्रकार छ तपाई को आजको राशिफल एस प्रकार छ</p>
							</div>
							<div class="clearfix"></div>

						</div>

						<div class="horoscope">

							<div class="horoscope-img">
								<img src="<?php echo $base_url;?>dist/img/leo.png" alt="leo" />
							</div>
							<div class="horoscope-detail">
								<h3>मेष</h3>
								<p >तपाई को आजको राशिफल एस प्रकार छ तपाई को आजको राशिफल एस प्रकार छ तपाई को आजको राशिफल एस प्रकार छ</p>
							</div>
							<div class="clearfix"></div>

						</div>

					</div> <!--end horoscope container-->
					
					<div class="horoscope-container item ">


						<div class="horoscope">

							<div class="horoscope-img">
								<img src="<?php echo $base_url;?>dist/img/leo.png" alt="leo" />
							</div>
							<div class="horoscope-detail">
								<h3>मेष</h3>
								<p>तपाई को आजको राशिफल एस प्रकार छ तपाई को आजको राशिफल एस प्रकार छ तपाई को आजको राशिफल एस प्रकार छ</p>
							</div>
							<div class="clearfix"></div>

						</div>

						<div class="horoscope">

							<div class="horoscope-img">
								<img src="<?php echo $base_url;?>dist/img/leo.png" alt="leo" />
							</div>
							<div class="horoscope-detail">
								<h3>मेष</h3>
								<p >तपाई को आजको राशिफल एस प्रकार छ तपाई को आजको राशिफल एस प्रकार छ तपाई को आजको राशिफल एस प्रकार छ</p>
							</div>
							<div class="clearfix"></div>

						</div>

						<div class="horoscope">

							<div class="horoscope-img">
								<img src="<?php echo $base_url;?>dist/img/leo.png" alt="leo" />
							</div>
							<div class="horoscope-detail">
								<h3>मेष</h3>
								<p >तपाई को आजको राशिफल एस प्रकार छ तपाई को आजको राशिफल एस प्रकार छ तपाई को आजको राशिफल एस प्रकार छ</p>
							</div>
							<div class="clearfix"></div>

						</div>

						<div class="horoscope">

							<div class="horoscope-img">
								<img src="<?php echo $base_url;?>dist/img/leo.png" alt="leo" />
							</div>
							<div class="horoscope-detail">
								<h3>मेष</h3>
								<p >तपाई को आजको राशिफल एस प्रकार छ तपाई को आजको राशिफल एस प्रकार छ तपाई को आजको राशिफल एस प्रकार छ</p>
							</div>
							<div class="clearfix"></div>

						</div>

					</div> <!--end horoscope container-->


				</div>	

			</div>

			
			<div id="model-carousel" class="carousel slide" data-ride="carousel">
				  <!-- Indicators -->
				  <ol class="carousel-indicators">
				    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				    <li data-target="#myCarousel" data-slide-to="1"></li>
				    <li data-target="#myCarousel" data-slide-to="2"></li>
				  </ol>

				  <!-- Wrapper for slides -->
				  <div class="carousel-inner">
				    <div class="item active">
				      <img src="<?php echo $base_url;?>dist/img/b5-30062017122333-0x825.jpg" alt="model">
				    </div>

				    <div class="item">
				       <img src="<?php echo $base_url;?>dist/img/b5-30062017122333-0x825.jpg" alt="model">
				    </div>

				    <div class="item">
				       <img src="<?php echo $base_url;?>dist/img/b5-30062017122333-0x825.jpg" alt="model">
				    </div>
				  </div>

				  <!-- Left and right controls -->
				  <a class="left carousel-control" href="#model-carousel" data-slide="prev">
				    <span class="glyphicon glyphicon-chevron-left"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="right carousel-control" href="#model-carousel" data-slide="next">
				    <span class="glyphicon glyphicon-chevron-right"></span>
				    <span class="sr-only">Next</span>
				  </a>
			</div><!--end model-carousel-->

			

		</div>

		
	</div>

	<div class="row">

		<img src="<?php echo $base_url;?>dist/img/advertisement4.png" alt="new_img" class="img-responsive advertisement4">

	</div>

	<div class="row">		
			
		<img src="http://via.placeholder.com/1350x350" class="img-responsive">
	
	</div>

</section>



<?php require_once("./include/footer.php") ?>