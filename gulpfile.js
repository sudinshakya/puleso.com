var gulp = require('gulp'),
	sass = require('gulp-sass'),
    uglify = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps'),
	livereload = require('gulp-livereload'),
	sassFile = './src/sass/**/*.scss',
	cssDest = './dist/css',
    jsFile = './src/js/*.js',
    jsDest = './dist/js';


//sass tasks
gulp.task('sass', function() {
    gulp.src(sassFile)
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(cssDest))        
        .pipe(livereload());
});


//js task
gulp.task('scripts', function() {
    return gulp.src(jsFile)
        .on('error', function(err) { gutil.log(err.message); })
        .pipe(sourcemaps.init({loadMaps: true}))        
        .pipe(uglify())
        .pipe(gulp.dest(jsDest))
        .pipe(livereload());;
});


//gulp watch for js and sass
gulp.task('watch', function () {
    livereload.listen();
    gulp.watch(sassFile, ['sass']);
 	gulp.watch(jsFile, ['scripts']);
	// gulp.watch(imgFile, ['img']);  
   
 
});


gulp.task('default', ['sass','scripts','watch'])
    function onError(err) {
      console.log(err);
      this.emit('end');
    }
