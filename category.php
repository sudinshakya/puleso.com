<?php require_once("./include/config.php") ?>
<?php require_once("./include/header.php") ?>
<?php require_once("./include/bread_crumb.php") ?>

<section id="category" class="container">

	<div class="row">
		<div class="col-sm-9">
			<div class="row left-featured bg-white">
				<div class="thumbnail-box">			
						<a href="#"	>	
					      <img src="<?php echo $base_url;?>dist/img/Dr.-KC-in-Hunger-Strike-9-826x495.jpg" alt="new_img" class="img-responsive">
					      </a>
					      <div class="caption">
					      	<a href="#" class="date-box" role="button"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;१ िदन अगािड</a>
					        <a href="#" target="_blank"><h4>केसीले आमरण अनसन, उपचार र पढार्इ नरोक्ने (िभिडयोसिहत)</h4></a>
					        <p class="caption-details">त्रिवि शिक्षण अस्पतालका प्रा. डा. गोविन्द केसीले सोमबार ११ औं आमरण अनसन सुरु गरेका छन् । सरकारले अनसन नबस्न अाग्रह गरे...अस्पतालमा पत्रकार सम्मेलन गरेर ...</p>	       
					      </div>

				</div>

			</div>
		
			<div class="row">
				<a href="#"	>	
				 <img src="<?php echo $base_url;?>dist/img/advertisement3.png" alt="new_img" class="advertisement3">
				</a>
			</div>	

			

			<div class="row left-featured bg-white entertainment">
				<div class="col-sm-12 featured-header">
					<h3 class="col-xs-6 ">मनोरंजन</h3>
					
				 </div>
				 <div class="col-xs-12" >				
					<div class="col-sm-4 thumbnail item">
						<a href="#"	>	
					      <img src="<?php echo $base_url;?>dist/img/ok-2-30062017122337-0x825.jpg" alt="new_img" class="img-responsive">
					      </a>
					      <div class="caption">
					      	<a href="#" class="date-box" role="button"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;१ िदन अगािड</a>
					        <a  href="#" target="_blank"><h4>केसीले आमरण अनसन, उपचार र पढार्इ नरोक्ने (िभिडयोसिहत)</h4></a>
					        <p>त्रिवि शिक्षण अस्पतालका प्रा. डा. गोविन्द केसीले सोमबार ११ औं आमरण अनसन सुरु गरेका छन् । सरकारले अनसन नबस्न अाग्रह गरे...अस्पतालमा पत्रकार सम्मेलन गरेर ...</p>	       
					      </div>
					</div>

					<div class="col-sm-4  thumbnail item">
					<a href="#"	>	
					      <img src="<?php echo $base_url;?>dist/img/ok-2-30062017122337-0x825.jpg" alt="new_img" class="img-responsive">
					      </a>
					      <div class="caption">
					      	<a href="#" class="date-box" role="button"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;१ िदन अगािड</a>
					        <a  href="#" target="_blank"> <h4>केसीले आमरण अनसन, उपचार र पढार्इ नरोक्ने (िभिडयोसिहत)</h4></a>
					        <p>त्रिवि शिक्षण अस्पतालका प्रा. डा. गोविन्द केसीले सोमबार ११ औं आमरण अनसन सुरु गरेका छन् । सरकारले अनसन नबस्न अाग्रह गरे...अस्पतालमा पत्रकार सम्मेलन गरेर ...</p>	       
					      </div>
					</div>

					<div class="col-sm-4  thumbnail item">
						<a href="#"	>	
					      <img src="<?php echo $base_url;?>dist/img/ok-2-30062017122337-0x825.jpg" alt="new_img" class="img-responsive">
					      </a>
					      <div class="caption">
					      	<a href="#" class="date-box" role="button"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;१ िदन अगािड</a>
					         <a  href="#" target="_blank"><h4>केसीले आमरण अनसन, उपचार र पढार्इ नरोक्ने (िभिडयोसिहत)</h4></a>
					        <p>त्रिवि शिक्षण अस्पतालका प्रा. डा. गोविन्द केसीले सोमबार ११ औं आमरण अनसन सुरु गरेका छन् । सरकारले अनसन नबस्न अाग्रह गरे...अस्पतालमा पत्रकार सम्मेलन गरेर ...</p>	       
					      </div>
					</div>
					
				 </div>
			
			</div>

			<div class="row left-featured bg-white entertainment">
				<div class="col-sm-12 featured-header">
					<h3 class="col-xs-6 ">मनोरंजन</h3>
					
				 </div>
				 <div class="col-xs-12" >				
					<div class="col-sm-6 thumbnail item">
						<a href="#"	>	
					      <img src="<?php echo $base_url;?>dist/img/ok-2-30062017122337-0x825.jpg" alt="new_img" class="img-responsive">
					      </a>
					      <div class="caption">
					      	<a href="#" class="date-box" role="button"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;१ िदन अगािड</a>
					        <a  href="#" target="_blank"><h4>केसीले आमरण अनसन, उपचार र पढार्इ नरोक्ने (िभिडयोसिहत)</h4></a>
					        <p>त्रिवि शिक्षण अस्पतालका प्रा. डा. गोविन्द केसीले सोमबार ११ औं आमरण अनसन सुरु गरेका छन् । सरकारले अनसन नबस्न अाग्रह गरे...अस्पतालमा पत्रकार सम्मेलन गरेर ...</p>	       
					      </div>
					</div>

					<div class="col-sm-6  thumbnail item">
						<a href="#"	>	
					      <img src="<?php echo $base_url;?>dist/img/ok-2-30062017122337-0x825.jpg" alt="new_img" class="img-responsive">
					      </a>
					      <div class="caption">
					      	<a href="#" class="date-box" role="button"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;१ िदन अगािड</a>
					        <a  href="#" target="_blank"> <h4>केसीले आमरण अनसन, उपचार र पढार्इ नरोक्ने (िभिडयोसिहत)</h4></a>
					        <p>त्रिवि शिक्षण अस्पतालका प्रा. डा. गोविन्द केसीले सोमबार ११ औं आमरण अनसन सुरु गरेका छन् । सरकारले अनसन नबस्न अाग्रह गरे...अस्पतालमा पत्रकार सम्मेलन गरेर ...</p>	       
					      </div>
					</div>
					
					
				 </div>
			
			</div>
		</div>		

		<div class="col-sm-3">				

			<div class="right-news business-category">
				<h4> &nbsp;&nbsp;Business Category</h4>
				<hr>
				<ul>
					<li><a href="#">Business</a></li>
					<li><a href="#">Stock Exchange</a></li>
					<li><a href="#">Import/Export</a></li>
					<li><a href="#">Budget</a></li>
					<li><a href="#">More Category</a></li>
					<span class="gradient-right"></span>
				</ul>


			</div>	
		

			<div class="right-news advertisement">
				<a href="#"	>	
				<img src="http://via.placeholder.com/350x650" class="add-image">
				</a>
			</div>

			
			<div class="right-news advertisement">
				<a href="#"	>	
				<img src="http://via.placeholder.com/350x650" class="add-image">
				</a>
			</div>

			<div class="right-news advertisement">
				<a href="#"	>	
				<img src="http://via.placeholder.com/350x650" class="add-image">
				</a>
			</div>
			

		</div>

		
	</div>

	<div class="row">
		<div class="text-center">
		<a class="btn btn-info btn-lg "><i class="fa fa-spinner loader" aria-hidden="true"></i>&nbsp;&nbsp;Load  More</a>
		</div>
	</div>



</section>



<?php require_once("./include/footer.php") ?>